const http = require("http");
const express = require("express");
const app = express();
const bodyparser = require("body-parser");

app.set('view engine',"ejs")
app.use(express.static(__dirname + '/public'))
app.use(bodyparser.urlencoded({extended:true}))

let datos = [{
            matricula:"2020030209",
            nombre:"HERNANDEZ IBARGUEN JUSTIN ADILAN",
            sexo:'M',
            materias:[" Ingles", " Tecnologia I", " Base de Datos"]
        },
        {
            matricula:"2020030309",
            nombre:"ACOSTA ORTEGA JESUS HUMBERTO",
            sexo:'M',
            materias:[" Ingles", " Tecnologia I", " Base de Datos"]
        },
        {
            matricula:"2020030309",
            nombre:"ACOSTA VARELA IRVING GUADALUPE",
            sexo:'M',
            materias:[" Ingles", " Tecnologia I", " Base de Datos"]
        },
        {
            matricula:"2020030319",
            nombre:"ALMOGAR VAZQUEZ YARLEN DE JESUS",
            sexo:'F',
            materias:[" Ingles", " Tecnologia I", " Base de Datos"]
        },
        {
            matricula:"2020030316",
            nombre:"SOTO OSUNA ALONDRA GUADALUPANA",
            sexo:'F',
            materias:[" Ingles", " Tecnologia I", " Base de Datos"]
        }
        ,
        {
            matricula:"2020030282",
            nombre:"PUGA VAZQUEZ ISSAC DAVID",
            sexo:'M',
            materias:[" Ingles", " Tecnologia I", " Base de Datos"]
        }
]
app.get('/',(req,res)=>{
    res.render('index',{titulo:"LISTADO DE ALUMNOS", listado:datos})
})

app.get('/tablas',(req,res)=>{
    const valores= {
        tabla:req.query.tabla
    }
    res.render('tablas',valores);
})

app.post('/tablas',(req,res)=>{
    const valores= {
        tabla:req.body.tabla
    }
    res.render('tablas',valores);
})
app.get('/cotizacion',(req,res)=>{
    const valores= {
        valor:req.query.valor,
        plazo:req.query.plazo,
        pInicial:req.query.pInicial,
        totalFin:req.query.totalFin,
        pagoInicial:req.query.pagoInicial,
        pagoMensual:req.query.pagoMensual
    }
    res.render('cotizacion',valores);
})

app.post('/cotizacion',(req,res)=>{
    const valores= {
        
        valor:req.body.valor,
        plazo:req.body.plazo,
        pInicial:req.body.pInicial,
        pagoInicial:req.body.pagoInicial,
        totalFin:req.body.totalFin,
        pagoMensual:req.body.pagoMensual
    }
    res.render('cotizacion',valores);
})
//Escuchar al servidor por el puerto 3000
app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html')
})


const puerto=3001;
//const ip='34.203.33.75';
    app.listen(puerto, ()=>{

        console.log("Iniciado puerto "+puerto);
    })
